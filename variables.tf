variable "resource_group_name" {
  type        = string
  description = "az resource group name"
}

variable "location" {
  type        = string
  description = "az resource location"
}

variable "account_tier" {
   type        = string
   description = "Storage access tier Standard or Premium" 
}

variable "account_replication_type" {
   type        = string
   description = "Storage replication type LRS or ZRS or GRS" 
}

variable "access_tier" {
  type         = string
  description  = "Storage access tier : values either Hot or Cool" 
}
  
  variable "access_type" {
  type         = string
  description  = "container access is either public or private"
  
}

variable "blob_type" {
  type         = string
  description  = "Type of blob storage : page or block or apend"
}

variable "filename" {
  type         = string
  description  = "source of the blob"
  default      = ""
}



