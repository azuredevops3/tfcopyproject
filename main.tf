# Azure provider source and version being used

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=2.26"
    }
  }
  required_version = ">= 0.14.9"
}
#configure provider
provider "azurerm" {
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "rg1" {
  name     = var.resource_group_name
  location = var.location
}

