output "devstgac" {
  value       = azurerm_storage_account.devstgac.name
  description = "Name of the storage account"
}
output "devstgcon" {
  value       = azurerm_storage_container.devstgcon.name
  description = "Name of the container"
}
output "devblob" {
  value       = azurerm_storage_blob.devblob.type
  description = "Type of blob storage"
}